import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { CodeService } from '../../services/code.service';
import { NgForm } from '@angular/forms';
import { MessageData } from '../../models/message.model';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  @ViewChild('f') form!: NgForm;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private codeService: CodeService,
    ) {}

  setFormValue(value: {[key: string] : any}) {
    setTimeout(() => {
      this.form.form.setValue(value);
    })
  }

  onEncodeMessage() {
    const messageData: MessageData = this.form.value;

      this.codeService.encodeMessage(messageData).subscribe(response => {
        this.setFormValue({
          password: this.form.value.password,
          decodedMessage: this.form.value.decodedMessage,
          encodedMessage: response.encoded,
        });
      });
  }

  onDecodeMessage() {
    const messageData: MessageData = this.form.value;

      this.codeService.decodeMessage(messageData).subscribe(response => {
        this.setFormValue({
          password: this.form.value.password,
          decodedMessage: response.decoded,
          encodedMessage: this.form.value.encodedMessage,
        });
      });
  }
}
