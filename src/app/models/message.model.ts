export class Message {
  constructor(
    public password: string,
    public decodedMessage: string,
    public encodedMessage: string
  ) {}
}

export interface MessageData {
  password: string;
  decodedMessage: string;
  encodedMessage: string;
}

export interface ResponseType {
  password: string;
  encoded: string;
  decoded: string;
}
