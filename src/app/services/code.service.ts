import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MessageData, ResponseType} from '../models/message.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CodeService {

  constructor(private http: HttpClient) { }

  encodeMessage(messageData: MessageData) {
    return this.http.post<ResponseType>(environment.apiUrl + '/encode', messageData);
  }

  decodeMessage(messageData: MessageData) {
    return this.http.post<ResponseType>(environment.apiUrl + '/decode', messageData);
  }
}
